﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Rectangle
    {
        private int _Height;

        public int Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        private int _Width;

        public int Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        private int _CalculateArea;

        public int Calculate
        {
            get { return _CalculateArea; }
            set { _CalculateArea = _Height * _Width; }
        }

        private int _CalculatePerimeter;

        public int CalculatePerimeter
        {
            get { return _CalculatePerimeter; }
            set { _CalculatePerimeter = _Height + _Width * 2; }
        }

    }
}
