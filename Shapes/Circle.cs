﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Circle
    {       

        

        private double _radius = 0;
        private int v;

        public Circle(double radius)
        {
            _radius = radius; 
        }

        public double Radius
        {
            get { return _radius; }
            set { _radius = value; }
        }

        public double Area()
        {
            double Area;
            Area = Math.PI * (_radius * _radius) ; 
            return Area;
        }

        public double Perimeter()
        {
            double perimeter;
            perimeter = (2 * Math.PI * _radius);
            return perimeter;
        }

    }
}
